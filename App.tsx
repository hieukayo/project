import * as React from 'react';
import {NavigationContainer, DrawerActions} from '@react-navigation/native';
import {View, Text, TouchableOpacity, Button} from 'react-native';
import {
  createStackNavigator,
  StackNavigationOptions,
} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import AddRoute from './Navigator/StackLogin';
import StackSettingScreen from './Navigator/StackSetting';
import login from './src/Login';

const TabNavigator = createBottomTabNavigator();

const TabNavigatorScreen: React.FunctionComponent = () => {
  return (
    <TabNavigator.Navigator>
      <TabNavigator.Screen name="login" component={AddRoute} />

      <TabNavigator.Screen name="Setting" component={StackSettingScreen} />
    </TabNavigator.Navigator>
  );
};

function HomeScreen1({}) {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Button
        onPress={() => console.log('Notifications')}
        title="Go to notifications"
      />
    </View>
  );
}

function NotificationsScreen({}) {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Button onPress={() => console.log('ahihi')} title="Go back home" />
    </View>
  );
}
const Drawer = createDrawerNavigator();

function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator>
        <Drawer.Screen
          name="kayo"
          component={TabNavigatorScreen}
          options={{
            drawerLabel: 'Reports',
          }}
        />
        <Drawer.Screen name="hien" component={AddRoute} />
        <Drawer.Screen name="tran" component={NotificationsScreen} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}
export default App;
