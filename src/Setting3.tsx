import React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import {RouteProp} from '@react-navigation/native';
import {SettingParam} from '../Navigator/StackSetting';
interface Props {
  route: RouteProp<SettingParam, 'Setting3'>;
}
class Setting3 extends React.Component<Props> {
  render() {
    return (
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity>
          <Text>
            move to screen 3 and say hello {this.props.route.params.name}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
export default Setting3;
