import {View, TouchableOpacity, Text} from 'react-native';
import React from 'react';
import {StackParam} from '../Navigator/StackLogin';
import {RouteProp, NavigationProp} from '@react-navigation/native';
interface Props {
  route: RouteProp<StackParam, 'home1'>;
  navigation: NavigationProp<StackParam, 'home1'>;
}
class Home extends React.Component<Props> {
  render() {
    return (
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <Text>ahihi xin chao {this.props.route.params.name}</Text>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.reset({
              index: 0,
              routes: [{name: 'Profile'}],
            });
          }}
          style={{backgroundColor: 'green'}}>
          <Text>OpenDrawer</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
export default Home;
