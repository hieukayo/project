import React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';
import {SettingParam} from '../Navigator/StackSetting';

interface Props {
  navigation: StackNavigationProp<SettingParam, 'Setting1'>;
}
class Setting1 extends React.Component<Props> {
  render() {
    return (
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('Setting2', {name: 'Setting 1'});
          }}>
          <Text>move to screen 2</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
export default Setting1;
