import React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';
import {SettingParam} from '../Navigator/StackSetting';
import {RouteProp} from '@react-navigation/native';
interface Props {
  navigation: StackNavigationProp<SettingParam, 'Setting2'>;
  route: RouteProp<SettingParam, 'Setting2'>;
}
class Setting2 extends React.Component<Props> {
  render() {
    return (
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('Setting3', {name: 'Setting3 hihi'});
          }}>
          <Text>
            move to screen 2 and say hello {this.props.route.params.name}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
export default Setting2;
