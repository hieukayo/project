import {View, TouchableOpacity, Text} from 'react-native';
import React from 'react';
import {StackNavigationProp} from '@react-navigation/stack';
import {StackParam} from '../Navigator/StackLogin';
interface Props {
  naigation: StackNavigationProp<StackParam, 'login1'>;
}
class InputData extends React.Component<Props> {
  render() {
    return (
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity
          onPress={() =>
            this.props.naigation.navigate('home1', {name: 'ngo thu hien'})
          }>
          <View>
            <Text>click here to login</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
export default InputData;
