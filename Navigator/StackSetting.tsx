import {createStackNavigator} from '@react-navigation/stack';
import Setting1 from '../src/Setting1';
import Setting2 from '../src/Setting2';
import Setting3 from '../src/Setting3';
import React from 'react';
export type SettingParam = {
  Setting1: undefined;
  Setting2: {name: string};
  Setting3: {name: string};
};
const StackSetting = createStackNavigator<SettingParam>();
const StackSettingScreen: React.FunctionComponent = () => {
  return (
    <StackSetting.Navigator initialRouteName="Setting1">
      <StackSetting.Screen name="Setting1" component={Setting1} />
      <StackSetting.Screen name="Setting2" component={Setting2} />
      <StackSetting.Screen name="Setting3" component={Setting3} />
    </StackSetting.Navigator>
  );
};
export default StackSettingScreen;
