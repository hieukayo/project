import {
  createStackNavigator,
  StackNavigationOptions,
} from '@react-navigation/stack';
import {StackNavigationProp} from '@react-navigation/stack';

import React from 'react';
import Home from '../src/Home';
import login from '../src/Login';
import {Button} from 'react-native';
import {DrawerActions} from '@react-navigation/native';
export type StackParam = {
  login1: undefined;
  home1: {name: string};
  Setting1: undefined;
};
const DefaultOption: StackNavigationOptions = {
  headerStyle: {
    backgroundColor: '#f4511e',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
  headerRight: () => (
    <Button
      onPress={() => DrawerActions.openDrawer()}
      title="openDrawer"
      color="red"
    />
  ),
  headerLeft: () => (
    <Button
      onPress={() => DrawerActions.openDrawer()}
      title="openDrawer"
      color="red"
    />
  ),
};

const Stack = createStackNavigator<StackParam>();
const AddRoute: React.FunctionComponent = () => {
  return (
    <Stack.Navigator initialRouteName="login1" screenOptions={DefaultOption}>
      <Stack.Screen name="home1" component={Home} />

      <Stack.Screen name="login1" component={login} />
    </Stack.Navigator>
  );
};
export default AddRoute;
